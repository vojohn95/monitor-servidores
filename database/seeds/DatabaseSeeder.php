<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name'=>'john',
            'email'=> 'lfernando@integrador-technology.mx',
            'password'=> Hash::make('Administrador1'),
        ]);

    }
}
